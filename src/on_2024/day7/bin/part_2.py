import pathlib as pl

import json

from collections.abc import Iterator


def combination(k: int) -> Iterator[tuple[int]]:
    if k == 1:
        for i in range(3): yield (i,)
    else:
        for i in range(3):
            for sub_out in combination(k - 1):
                out = [i]
                out.extend(sub_out)
                yield tuple(out)


def solution(path_src: pl.Path) -> int:
    with open(path_src, mode = "r", encoding = "utf-8") as in_stream:
        lines: list[str] = in_stream.read().split("\n")[:-1]

    result: int = 0

    for line in lines:
        out, rest = line.split(": ")
        out = int(out)
        operands = [int(operand) for operand in rest.split(" ")]

        for combi in combination(len(operands) - 1):
            res = operands[0]
            for op, operand in zip(combi, operands[1:]):
                if op == 0: res += operand
                elif op == 1: res *= operand
                elif op == 2: res = int(str(res) + str(operand))
            if res == out:
                result += out
                break

    return result


def main():
    with open("../data/meta.json", mode = "r", encoding = "utf-8") as in_stream:
        meta = json.load(in_stream)

    # input for test case
    path_test: pl.Path = pl.Path(f'../data/{meta["test"]["part"]["2"]}')
    expected_result_test: int = meta["test"]["expected_result"]["2"]

    # actual input of the day
    path_input: pl.Path = pl.Path(f"../data/{meta['input']}")

    # exec
    if meta["test"]["exec"]:
        result_test: int = solution(path_src = path_test)
        if result_test != expected_result_test:
            print(f"result:   {result_test}")
            print(f"expected: {expected_result_test}")
            raise AssertionError("!")
        else: print("test successful")
    else:
        result: int = solution(path_src = path_input)
        print(f"answer for part_1 is: {result}")


if __name__ == "__main__": main()
