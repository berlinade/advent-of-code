import pathlib as pl

import json


def identifiy_digit(idx: int, line) -> tuple[int, int] | None:
    number: str = ""
    while (idx < len(line)) and (line[idx] in "0123456789"):
        number += line[idx]
        idx += 1
    if (len(number) > 0) and (len(number) < 4): return idx, int(number)
    return None


def scan_line(line: str):
    idx: int = 0
    keep_on: bool = len(line) > 0
    pairs: list[tuple[int, ...]] = list()
    while keep_on:
        char: str = line[idx]
        if char == "m":
            jdx = idx + 4
            pair: list[int] = list()
            if line[idx:jdx] == "mul(":
                resp = identifiy_digit(jdx, line)
                if not (resp is None):
                    jdx = resp[0]
                    pair.append(resp[1])
                    if line[jdx] == ",":
                        jdx += 1
                        resp = identifiy_digit(jdx, line)
                        if not (resp is None):
                            jdx = resp[0]
                            pair.append(resp[1])
                            if line[jdx] == ")":
                                pairs.append(tuple(pair))
                                jdx += 1
                idx = jdx - 1
        idx += 1
        if idx >= len(line): keep_on = False
    summed_up: int = 0
    for pair in pairs:
        if len(pair) != 2: raise AssertionError("!")
        summed_up += pair[0]*pair[1]
    return summed_up, pairs


def solution(path_src: pl.Path) -> int:
    with open(path_src, mode = "r", encoding = "utf-8") as in_stream:
        lines: list[str] = in_stream.read().split("\n")[:-1]

    result: int = 0
    for line in lines:
        summed_up_line, pairs = scan_line(line)
        # print(pairs)
        # print(result, summed_up_line)
        result += summed_up_line

    return result


def main():
    with open("../data/meta.json", mode = "r", encoding = "utf-8") as in_stream:
        meta = json.load(in_stream)

    # input for test case
    path_test: pl.Path = pl.Path(f'../data/{meta["test"]["part"]["1"]}')
    expected_result_test: int = meta["test"]["expected_result"]["1"]

    # actual input of the day
    path_input: pl.Path = pl.Path(f"../data/{meta['input']}")

    # exec
    if meta["test"]["exec"]:
        result_test: int = solution(path_src = path_test)
        if result_test != expected_result_test:
            print(f"result:   {result_test}")
            print(f"expected: {expected_result_test}")
            raise AssertionError("!")
        else: print("test successful")
    else:
        result: int = solution(path_src = path_input)
        print(f"answer for part_1 is: {result}")


if __name__ == "__main__": main()
