import pathlib as pl

import json


class WordMatrix(object):
    width: int
    height: int
    mat: list[str]

    def __init__(self, matrix: list[str]):
        self.height = len(matrix)
        self.width = len(matrix[0])
        self.mat = matrix

        for line in self.mat:
            if len(line) != self.width: raise AssertionError("!")

    def __getitem__(self, coord: tuple[int, int]) -> str | None:
        i, j = coord
        if i < 0: return None
        if j < 0: return None
        if i >= self.height: return None
        if j >= self.width: return None
        return self.mat[i][j]

    def contains(self, i: int, j: int, direction: tuple[int, int], word: str) -> bool:
        for letter in word:
            if letter != self[i, j]: return False
            i += direction[0]
            j += direction[1]
        return True


def solution(path_src: pl.Path) -> int:
    with open(path_src, mode = "r", encoding = "utf-8") as in_stream:
        lines: list[str] = in_stream.read().split("\n")[:-1]

    result: int = 0

    wm = WordMatrix(lines)
    for i in range(wm.height):
        for j in range(wm.width):
            if wm[i, j] == "X":
                for direction in [
                    (0, 1), (0, -1),
                    (1, 0), (-1, 0),
                    (1, 1), (-1, -1),
                    (-1, 1), (1, -1)
                ]:
                    if wm.contains(i, j, direction, "XMAS"): result += 1

    return result


def main():
    with open("../data/meta.json", mode = "r", encoding = "utf-8") as in_stream:
        meta = json.load(in_stream)

    # input for test case
    path_test: pl.Path = pl.Path(f'../data/{meta["test"]["part"]["1"]}')
    expected_result_test: int = meta["test"]["expected_result"]["1"]

    # actual input of the day
    path_input: pl.Path = pl.Path(f"../data/{meta['input']}")

    # exec
    if meta["test"]["exec"]:
        result_test: int = solution(path_src = path_test)
        if result_test != expected_result_test:
            print(f"result:   {result_test}")
            print(f"expected: {expected_result_test}")
            raise AssertionError("!")
        else: print("test successful")
    else:
        result: int = solution(path_src = path_input)
        print(f"answer for part_1 is: {result}")


if __name__ == "__main__": main()
