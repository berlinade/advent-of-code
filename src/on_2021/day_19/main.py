import pickle

from itertools import permutations, product

from typing import Union, Callable, Optional, TypeAlias


with open('input_a_ex', mode = 'r') as fStream: RAW_ex: str = fStream.read()


pos_3d_t: TypeAlias = tuple[int, int, int]
axis_t: TypeAlias = str
rotation_t: TypeAlias = tuple[axis_t, axis_t, axis_t]
rotation_list_t: TypeAlias = list[rotation_t]
seeing_t: TypeAlias = dict['Scan', dict['Scan', dict[str, Union[rotation_list_t, list[int]]]]]


def rot_interpreter(axis: axis_t) -> tuple[int, int]:
    sigma: int = -1 if '-' in axis else 1
    axis_num: int = 0
    if 'y' in axis: axis_num = 1
    if 'z' in axis: axis_num = 2
    return sigma, axis_num


def create_90_degree_rotations() -> rotation_list_t:
    rot: set = set()
    for (new_x, new_y, new_z) in permutations(('x', 'y', 'z'), 3):
        rot.add((new_x, new_y, new_z))
        rot.add((f'-{new_x}', new_y, new_z))
        rot.add((new_x, f'-{new_y}', f'-{new_z}'))
        rot.add((new_x, f'-{new_y}', new_z))
        rot.add((f'-{new_x}', new_y, f'-{new_z}'))
        rot.add((new_x, new_y, f'-{new_z}'))
        rot.add((f'-{new_x}', f'-{new_y}', new_z))
        rot.add((f'-{new_x}', f'-{new_y}', f'-{new_z}'))
    return list(rot)


class Scan(object):
    name: str = None
    beacons: list[pos_3d_t] = None
    offset: pos_3d_t = None

    def __init__(self, name: str, beacons: list[pos_3d_t]):
        self.name = name
        self.beacons = beacons
        self.offset = (0, 0, 0)

    def beacons_rotated(self, rot: rotation_t) -> list[pos_3d_t]:
        _rot_beacons: list[pos_3d_t] = []
        for beacon in self.beacons:
            new_beacon_pos = []
            for axis in rot:
                sigma, idx = rot_interpreter(axis)
                new_beacon_pos.append(sigma*beacon[idx])
            _rot_beacons.append(tuple(new_beacon_pos))
        return _rot_beacons

    def beacons_rel(self, rot: Optional[rotation_t] = None) -> dict[pos_3d_t, int]:
        if not (rot is None): beacons = self.beacons_rotated(rot)
        else: beacons = self.beacons
        out: dict[pos_3d_t, int] = dict()
        for beacon in beacons:
            for other_beacon in beacons:
                if beacon is other_beacon: continue
                diff = tuple(entry - other_entry for entry, other_entry in zip(beacon, other_beacon))
                if diff in out: out[diff] += 1
                else: out[diff] = 1
        return out

    def __str__(self) -> str: return self.name

    def __repr__(self) -> str: return self.name


def scans_raw_to_dict(raw: str) -> list[Scan]:
    scans: list[Scan] = []

    for idx, block in enumerate(raw.split('\n\n')):
        lines = block.splitlines()
        new_coords: list[pos_3d_t] = []
        scans.append(Scan((name := lines[0].split('---')[1].strip()), new_coords))
        if int(name.split()[1]) != idx: raise AssertionError
        for line in lines[1:]:
            coords: list[str] = line.split(',')
            new_coords.append(tuple(int(entry) for entry in coords))

    return scans


def seeing(scan1: Scan, scan2: Scan, verbose: bool = False):
    rotations_map = create_90_degree_rotations()

    beacons1 = scan1.beacons
    rot, matches_1, matches_2, offset_return = ('', '', ''), set(), set(), None
    beacons1_rel = scan1.beacons_rel()  # for fast check
    for _idx_rot, rot in enumerate(rotations_map):
        if verbose: print(_idx_rot, rot)
        beacons2 = scan2.beacons_rotated(rot)

        ''' fast check '''
        beacons2_rel = scan2.beacons_rel(rot)  # for fast check
        fast_matches: int = 0
        for (key1, val1) in beacons1_rel.items():
            for (key2, val2) in beacons2_rel.items():
                if key2 == key1: fast_matches += min(val2, val1)
        if fast_matches < 30: continue

        for (idx_o_1, beacon1_origin), (idx_o_2, beacon2_origin) in product(enumerate(beacons1[:-11]),
                                                                            enumerate(beacons2[:-11])):
            offset_origin = tuple(entry1 - entry2 for entry1, entry2 in zip(beacon1_origin, beacon2_origin))
            matches_1: set[int] = set()
            matches_2: set[int] = set()
            for (idx_1, beacon1), (idx_2, beacon2) in product(enumerate(beacons1), enumerate(beacons2)):
                offset = tuple(entry1 - entry2 for entry1, entry2 in zip(beacon1, beacon2))
                if offset == offset_origin:
                    matches_1.add(idx_1)
                    matches_2.add(idx_2)
                if (len(matches_1) >= 12) and (offset_return is None): offset_return = offset
                if idx_1 == len(beacon1) - 12 + len(matches_1): break

            if len(matches_1) >= 12: break
        else: continue
        break
    if len(matches_1) < 12: rot = None
    return rot, matches_1, matches_2, offset_return  # "offset_return" also is relative position of scan2 from scan1!


class _handler(object):

    def __init__(self, data):
        self.seen = set()
        self._to_see = []
        self._rest = list(data)

    def append_to_see(self, entry) -> None:
        self._rest = [other_entry for other_entry in self._rest if entry != other_entry]
        self._to_see = [other_entry for other_entry in self._to_see if entry != other_entry]
        if entry in self.seen: return None
        self._to_see.append(entry)

    def __iter__(self): return self

    def __next__(self):
        if len(self._to_see) > 0: _next = self._to_see.pop(0)
        elif len(self._rest) > 0: _next = self._rest.pop(0)
        else: raise StopIteration()
        self._to_see = [other_entry for other_entry in self._to_see if _next != other_entry]
        self._rest = [other_entry for other_entry in self._rest if _next != other_entry]
        self.seen.add(_next)
        return _next


def namer(prefix: str):
    p_name: str = f'_{prefix}_tmp.pkl'
    b_name: str = '_all' + p_name
    return p_name, b_name


def loader(prefix: str,
           scans: Optional[list[Scan]] = None,
           update: Optional[bool] = None,
           must_succeed: bool = False):
    p_name, b_name = namer(prefix)

    all_beacons = None
    if (not update) or must_succeed:
        try:
            with open(p_name, mode = 'rb') as pStream: scans = pickle.load(pStream)
            with open(b_name, mode = 'rb') as bStream: all_beacons = pickle.load(bStream)
        except FileNotFoundError as e:
            if must_succeed: raise e from None
            update = True
    return update, scans, all_beacons


def _dumper(prefix: str, scans: Optional = None, all_beacons: Optional = None) -> None:
    p_name, b_name = namer(prefix)
    with open(p_name, mode = 'wb') as pStream: pickle.dump(scans, pStream)
    with open(b_name, mode = 'wb') as bStream: pickle.dump(all_beacons, bStream)


def count_beacons(scans: list[Scan], prefix: str, update: bool = False):
    update, scans, all_beacons = loader(prefix, scans, update)

    if update:
        all_beacons: set[pos_3d_t] = set(scans[0].beacons)
        outer = _handler(range(len(scans)))
        outer.append_to_see(0)
        for idx1 in outer:
            scan1 = scans[idx1]
            for idx2 in range(len(scans)):
                scan2 = scans[idx2]
                if (idx1 is idx2) or (idx2 in outer.seen): continue
                print(scan1, scan2, end = ' ')
                rot, matches_1, matches_2, offset = seeing(scan1, scan2, verbose = False)
                print(len(matches_1), len(matches_2))
                if len(matches_1) >= 12:
                    outer.append_to_see(idx2)
                    scan2.beacons = [tuple(entry + offs for entry, offs in zip(beacon, offset))
                                     for beacon in scan2.beacons_rotated(rot)]
                    scan2.offset = offset
                    all_beacons.update(scan2.beacons)
        _dumper(prefix, scans, all_beacons)

    out = len(all_beacons)
    return out


def max_dist(prefix: str, debug: bool = False) -> int:
    update, scans, all_beacons = loader(prefix, scans = [], update = False, must_succeed = True)
    all_beacons = list(all_beacons)

    if debug:
        with open('input_b_ex', mode = 'r') as fStream: RAW_ex_b = fStream.read()
        all_beacons_check = [tuple(int(entry) for entry in line.split(',')) for line in RAW_ex_b.splitlines()]
        print(all_beacons_check)

        print(len(all_beacons), len(all_beacons_check))
        for beacon in all_beacons:
            if beacon in all_beacons_check:
                print(f'check {beacon}')
                all_beacons_check.remove(beacon)
            else: print(f'FAIL! {beacon}')

    _max_dist: int = 0
    for idx, scan in enumerate(scans[:-1]):
        for other_scan in scans[idx + 1:]:
            _dist: int = sum(abs(entry - other_entry) for entry, other_entry in zip(scan.offset, other_scan.offset))
            _max_dist = max(_max_dist, _dist)
    return _max_dist


def main():
    '''
        -- part one --
    '''
    scans_ex: list[Scan] = scans_raw_to_dict(RAW_ex)
    assert count_beacons(scans_ex, prefix = 'ex', update = False) == 79  # , _pre_map = {0: (1,), 1: (3, 4), 2: (4,), 3: (4,)}))

    with open('input_a', mode = 'r') as fStream: RAW = fStream.read()
    scans: list[Scan] = scans_raw_to_dict(RAW)
    print(count_beacons(scans, prefix = 'task', update = False))

    '''
        -- part two --
    '''
    assert max_dist(prefix = 'ex') == 3621

    print(max_dist(prefix = 'task'))


if __name__ == '__main__': main()
