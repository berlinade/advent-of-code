from typing import Union, Optional, Callable


RAW_ex: str = '''NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C
'''


def preprocess_raw(raw: str) -> tuple[str, list[str]]:
    lines_raw: list[str] = raw.splitlines()
    start_chem: str = lines_raw[0]
    lines: list[str] = lines_raw[2:]
    return start_chem, lines


def create_insert_lookup(lines: list[str]) -> dict[str, str]:
    insert_lookup: dict[str, str] = {}
    for line in lines:
        reaction: list[str] = line.split(' -> ')
        if reaction[0] in insert_lookup: raise AssertionError('!')
        insert_lookup[reaction[0]] = reaction[1]
    return insert_lookup


class Chem(object):
    insert_lookup: dict[str, str] = None
    data: dict[str, int] = None
    leading_letter: str = None

    def __init__(self, chem: str, insert_lookup: dict[str, str]):
        self.insert_lookup = insert_lookup
        self.data = dict()
        for letter_1, letter_2 in zip(chem, chem[1:]):
            chem_pair: str = self._chem_pair(letter_1, letter_2)
            self.data[chem_pair] = self.data.get(chem_pair, 0) + 1
        self.leading_letter = chem[0]

    @staticmethod
    def _chem_pair(letter_1: str, letter_2: str) -> str:
        assert len(letter_1) == 1
        assert len(letter_2) == 1
        return ''.join((letter_1, letter_2))

    def _process_chem_pair(self, chem_pair: str) -> tuple[str, str]:
        new_letter: str = self.insert_lookup[chem_pair]
        new_chem_pair_left = self._chem_pair(chem_pair[0], new_letter)
        new_chem_pair_right = self._chem_pair(new_letter, chem_pair[1])
        return new_chem_pair_left, new_chem_pair_right

    def process(self) -> None:
        new_data: dict[str, int] = dict()
        for chem_pair, num in self.data.items():
            chem_left, chem_right = self._process_chem_pair(chem_pair)
            new_data[chem_left] = new_data.get(chem_left, 0) + self.data[chem_pair]
            new_data[chem_right] = new_data.get(chem_right, 0) + self.data[chem_pair]
        self.data = new_data

    def _count(self) -> list[int]:
        letter_occ: dict[str, int] = {self.leading_letter: 1}
        for chem_pair, num in self.data.items():
            letter: str = chem_pair[1]
            letter_occ[letter] = letter_occ.get(letter, 0) + num
        return list(letter_occ.values())

    def analyze(self) -> int:
        occ: list[int] = self._count()
        return max(occ) - min(occ)


def main():
    '''
        -- resources & part one --
    '''
    chem_ex, lines_ex = preprocess_raw(RAW_ex)
    insert_lookup_ex = create_insert_lookup(lines_ex)
    chem_inst_ex: Chem = Chem(chem_ex, insert_lookup_ex)
    chem_inst_ex.process()
    for chem_pair, num in (('NC', 1), ('CN', 1), ('NB', 1), ('BC', 1), ('CH', 1), ('HB', 1)):
        assert chem_inst_ex.data[chem_pair] == num
    for _ in range(9): chem_inst_ex.process()
    assert chem_inst_ex.analyze() == 1588

    with open('input_a', mode = 'r') as fStream: RAW = fStream.read()
    chem, lines = preprocess_raw(RAW)
    insert_lookup = create_insert_lookup(lines)
    chem_inst: Chem = Chem(chem, insert_lookup)
    for _ in range(10): chem_inst.process()
    print(chem_inst.analyze())

    '''
        -- part two --
    '''
    for _ in range(30): chem_inst_ex.process()
    assert chem_inst_ex.analyze() == 2188189693529

    for _ in range(30): chem_inst.process()
    print(chem_inst.analyze())


if __name__ == '__main__': main()
