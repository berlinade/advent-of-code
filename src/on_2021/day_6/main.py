from typing import TypeAlias


RAW_ex = '''3,4,3,1,2
'''


class LanternFish(object): pass


class School(LanternFish):
    data: dict[int, int]

    def __init__(self):
        self.data = {i: 0 for i in range(9)}

    def day(self, amount: int = 1) -> None:
        for single_day in range(amount): self._single_day()

    def _single_day(self) -> None:
        six: int = self.data[0]
        for i in range(1, 9):
            self.data[i - 1] = self.data[i]
        self.data[6] += six
        self.data[8] = six

    def population(self) -> int:
        out: int = 0
        for key, val in self.data.items(): out += val
        return out

    def __str__(self) -> str: return str(self.data)


def init_school(raw: str) -> School:
    init_population = [int(entry) for entry in raw.strip().split(',')]
    school_instance = School()
    for fish in init_population: school_instance.data[fish] += 1
    return school_instance


def main():
    '''
        -- resources --
    '''
    school_ex = init_school(RAW_ex)
    with open('input_a', mode = 'r') as fStream:
        RAW = fStream.read()
    school = init_school(RAW)

    '''
        -- part one & two --
    '''
    school_ex.day(18)
    assert school_ex.population() == 26  # part one
    school_ex.day(80 - 18)
    assert school_ex.population() == 5_934  # part one
    school_ex.day(256 - 80)
    assert school_ex.population() == 26_984_457_539  # part two

    '''
        -- part one & two --
    '''
    school.day(80)
    print(school.population())  # part one
    school.day(256 - 80)
    print(school.population())  # part two


if __name__ == '__main__': main()
