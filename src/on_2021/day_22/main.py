from itertools import product

from typing import TypeAlias, Union, Optional


RAW_ex_debug: str = '''on x=10..12,y=10..12,z=10..12
on x=11..13,y=11..13,z=11..13
off x=9..11,y=9..11,z=9..11
on x=10..10,y=10..10,z=10..10
'''


RAW_ex: str = '''on x=-20..26,y=-36..17,z=-47..7
on x=-20..33,y=-21..23,z=-26..28
on x=-22..28,y=-29..23,z=-38..16
on x=-46..7,y=-6..46,z=-50..-1
on x=-49..1,y=-3..46,z=-24..28
on x=2..47,y=-22..22,z=-23..27
on x=-27..23,y=-28..26,z=-21..29
on x=-39..5,y=-6..47,z=-3..44
on x=-30..21,y=-8..43,z=-13..34
on x=-22..26,y=-27..20,z=-29..19
off x=-48..-32,y=26..41,z=-47..-37
on x=-12..35,y=6..50,z=-50..-2
off x=-48..-32,y=-32..-16,z=-15..-5
on x=-18..26,y=-33..15,z=-7..46
off x=-40..-22,y=-38..-28,z=23..41
on x=-16..35,y=-41..10,z=-47..6
off x=-32..-23,y=11..30,z=-14..3
on x=-49..-5,y=-3..45,z=-29..18
off x=18..30,y=-20..-8,z=-3..13
on x=-41..9,y=-7..43,z=-33..15
on x=-54112..-39298,y=-85059..-49293,z=-27449..7877
on x=967..23432,y=45373..81175,z=27513..53682
'''


class Vol3d(object):
    x: list[int] = None
    y: list[int] = None
    z: list[int] = None
    part_2_mode: bool = None

    def __init__(self, x: list[int], y: list[int], z: list[int], part_2_mode: bool = False):
        self.x = x
        self.y = y
        self.z = z
        self.part_2_mode = part_2_mode

    def __str__(self) -> str:
        return f'[x: {self.x[0]}, {self.x[1]}|y: {self.y[0]}, {self.y[1]}|z: {self.z[0]}, {self.z[1]}|vol: {self.vol}]'

    def __repr__(self) -> str:
        return f'Vol3d(x = {self.x}, y = {self.y}, z = {self.z}, part_2_mode = {self.part_2_mode})'

    @property
    def vol(self) -> int:
        if self.part_2_mode:
            return max(0, self.x[1] - self.x[0])*max(0, self.y[1] - self.y[0])*max(0, self.z[1] - self.z[0])
        return max(0, self.x[1] - self.x[0] + 1)*max(0, self.y[1] - self.y[0] + 1)*max(0, self.z[1] - self.z[0] + 1)

    def intersection(self, other: 'Vol3d') -> 'Vol3d':
        assert self.part_2_mode == other.part_2_mode
        return Vol3d([max(self.x[0], other.x[0]), min(self.x[1], other.x[1])],
                     [max(self.y[0], other.y[0]), min(self.y[1], other.y[1])],
                     [max(self.z[0], other.z[0]), min(self.z[1], other.z[1])], part_2_mode = other.part_2_mode)

    def includes(self, other: 'Vol3d') -> bool:
        assert self.part_2_mode == other.part_2_mode
        if self.x[0] > other.x[0]: return False
        if self.x[1] < other.x[1]: return False
        if self.y[0] > other.y[0]: return False
        if self.y[1] < other.y[1]: return False
        if self.z[0] > other.z[0]: return False
        if self.z[1] < other.z[1]: return False
        return True

    @staticmethod
    def merge(arg: list['Vol3d']) -> list['Vol3d']:
        while True:
            _removal_list: list[Vol3d] = []
            _new_entries: list[Vol3d] = []
            for idx, entry in enumerate(arg[:-1]):
                assert entry.vol > 0
                for other in arg[idx + 1:]:
                    assert entry.part_2_mode == other.part_2_mode
                    assert other.vol > 0
                    x_is_x: bool = entry.x == other.x
                    y_is_y: bool = entry.y == other.y
                    z_is_z: bool = entry.z == other.z
                    if x_is_x and y_is_y and (entry.z[1] >= other.z[0]) and (other.z[1] >= entry.z[0]):
                        _removal_list.extend((entry, other))
                        _new_entries.append(Vol3d(entry.x,
                                                  entry.y,
                                                  [min(entry.z[0], other.z[0]), max(entry.z[1], other.z[1])],
                                                  part_2_mode = entry.part_2_mode))
                        break
                    elif x_is_x and (entry.y[1] >= other.y[0]) and (other.y[1] >= entry.y[0]) and z_is_z:
                        _removal_list.extend((entry, other))
                        _new_entries.append(Vol3d(entry.x,
                                                  [min(entry.y[0], other.y[0]), max(entry.y[1], other.y[1])],
                                                  entry.z,
                                                  part_2_mode = entry.part_2_mode))
                        break
                    elif (entry.x[1] >= other.x[0]) and (other.x[1] >= entry.x[0]) and y_is_y and z_is_z:
                        _removal_list.extend((entry, other))
                        _new_entries.append(Vol3d([min(entry.x[0], other.x[0]), max(entry.x[1], other.x[1])],
                                                  entry.y,
                                                  entry.z,
                                                  part_2_mode = entry.part_2_mode))
                        break  # prevents continue underneath
                else: continue  # leads to while breaking break
                break  # prevents breaking while
            else: break  # escaping while
            for entry in _removal_list: arg.remove(entry)
            for entry in _new_entries: arg.append(entry)
        return arg

    def shave_off(self, other: 'Vol3d', return_intersection: bool = False):
        assert self.part_2_mode
        assert other.part_2_mode
        out: list[Vol3d] = []
        x_series = [self.x[0], self.x[1], other.x[0], other.x[1]]
        y_series = [self.y[0], self.y[1], other.y[0], other.y[1]]
        z_series = [self.z[0], self.z[1], other.z[0], other.z[1]]
        for x1, y1, z1, x2, y2, z2 in product(x_series, y_series, z_series, x_series, y_series, z_series):
            if x1 >= x2: continue
            if y1 >= y2: continue
            if z1 >= z2: continue
            c = Vol3d([x1, x2], [y1, y2], [z1, z2], part_2_mode = True)
            # if c.vol <= 0: continue
            if not (self.includes(c)): continue
            if other.intersection(c).vol > 0: continue
            for entry in out:
                if c.intersection(entry).vol > 0: break
            else: out.append(c)
        self.merge(out)
        if return_intersection: out.append(self.intersection(other))
        return out


reboot_cmds_t: TypeAlias = list[tuple[bool, Vol3d]]


def convert_to_reboot_cmds(raw: str, part_2_mode: bool = False) -> reboot_cmds_t:
    out: reboot_cmds_t = []
    for line in raw.splitlines():
        parts = line.split()
        turn_on: bool = True if parts[0] == 'on' else False
        parts = parts[1].split(',')
        args = []
        for part in parts:
            part = part.split('=')[1]
            args.append([int(entry) for entry in part.split('..')])
            if part_2_mode: args[-1][1] += 1  # to make cube exclusive
        out.append((turn_on, Vol3d(*args, part_2_mode = part_2_mode)))
    return out


def determine_volume(vols: list[Vol3d]) -> int:
    volume: int = 0
    for entry in vols:
        new_volume: int = entry.vol
        assert new_volume > 0
        volume += new_volume
    return volume


def exec_reboot_cmds(reboot_cmds: reboot_cmds_t, focus: Optional[Vol3d] = None, verbose: bool = False) -> int:
    vols: list[Union[Vol3d, None]] = []
    for on, vol in reboot_cmds:
        if verbose: print(f'{on}, {vol}', end = ' ')
        if not (focus is None):
            vol = focus.intersection(vol)
            if vol.vol <= 0: continue
        to_dos: list[Vol3d] = []
        for idx, entry in enumerate(vols):
            if vol.intersection(entry).vol > 0:
                to_dos.append(entry)
                vols[idx] = None
        if on: vols.append(vol)
        for to_do in to_dos: vols.extend(to_do.shave_off(vol))
        vols = [entry for entry in vols if not (entry is None)]

        if verbose: print(f'volume: {determine_volume(vols)}')
    return determine_volume(vols)


def main():
    '''
        -- debug --
    '''
    reboot_cmds_ex_debug: reboot_cmds_t = convert_to_reboot_cmds(RAW_ex_debug, part_2_mode = True)
    assert exec_reboot_cmds(reboot_cmds_ex_debug, verbose = False) == 39

    '''
        -- part one --
    '''
    reboot_cmds_ex: reboot_cmds_t = convert_to_reboot_cmds(RAW_ex, part_2_mode = True)
    assert exec_reboot_cmds(reboot_cmds_ex,
                            focus = Vol3d([-50, 51], [-50, 51], [-50, 51], part_2_mode = True),
                            verbose = False) == 590784

    with open('input_a', mode = 'r') as fStream: RAW = fStream.read()
    reboot_cmds: reboot_cmds_t = convert_to_reboot_cmds(RAW, part_2_mode = True)
    print(exec_reboot_cmds(reboot_cmds,
                           focus = Vol3d([-50, 51], [-50, 51], [-50, 51], part_2_mode = True),
                           verbose = False))

    '''
        -- part two --
    '''
    with open('input_a', mode = 'r') as fStream: RAW = fStream.read()
    reboot_cmds: reboot_cmds_t = convert_to_reboot_cmds(RAW, part_2_mode = True)
    print(exec_reboot_cmds(reboot_cmds, verbose = False))


if __name__ == '__main__': main()
