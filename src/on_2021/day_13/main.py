import numpy as np

from typing import Optional


RAW_ex: str = '''6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5
'''


def preprocess_raw(raw: str) -> tuple[list[str], list[dict[str, int]]]:
    lines_raw: list[str] = raw.splitlines()
    lines: list[str] = []
    _folds: list[str] = []
    _tmp = lines
    for line_raw in lines_raw:
        if line_raw == '':
            _tmp = _folds
            continue
        _tmp.append(line_raw)
    folds: list[dict[str, int]] = []  # e.g. [{'y': 7}, {'x': 5}, ...]
    for line in _folds:
        _presplit: list[str] = line.split(' ')[-1].split('=')
        folds.append({_presplit[0] : int(_presplit[1])})
    return lines, folds


def raw_to_mat(lines: list[str]) -> np.ndarray:
    coords: list[tuple[int, ...]] = []
    max_x, max_y = 0, 0
    for line in lines:
        coords.append(tuple(int(entry) for entry in line.split(',')))
        assert len(coords[-1]) == 2
        max_x = max(max_x, coords[-1][0] + 1)
        max_y = max(max_y, coords[-1][1] + 1)
    out = np.zeros(shape = (max_y, max_x), dtype = bool)
    for x, y in coords:
        out[y, x] = True
    return out


def prettyprint(mat: np.ndarray) -> None:
    out: str = ''
    for i in range(mat.shape[0]):
        out += '\n'
        for j in range(mat.shape[1]):
            out += '#' if mat[i, j] else '.'
    print(out)


def fold(mat: np.ndarray, x: Optional[int] = None, y: Optional[int] = None):
    if x is None:
        if y is None: raise AssertionError('!')
        return _fold_y(mat, y)
    if y is None: return _fold_x(mat, x)
    raise AssertionError('!')


def _fold_y(mat: np.ndarray, y: int) -> np.ndarray:
    sub_mat_a: np.ndarray = mat[:y, :]
    sub_mat_b: np.ndarray = mat[y + 1:, :][::-1, :]
    sub_mat_a[-sub_mat_b.shape[0]:, :] += sub_mat_b
    return sub_mat_a


def _fold_x(mat: np.ndarray, x: int) -> np.ndarray:
    sub_mat_a: np.ndarray = mat[:, :x]
    sub_mat_b: np.ndarray = mat[:, x + 1:][:, ::-1]
    sub_mat_a[:, -sub_mat_b.shape[1]:] += sub_mat_b
    return sub_mat_a


def main():
    '''
        -- resources & part one --
    '''
    lines_ex, folds_ex = preprocess_raw(RAW_ex)
    mat_ex = raw_to_mat(lines_ex)
    mat_ex = fold(mat_ex, **folds_ex[0])
    assert np.sum(mat_ex) == 17

    with open('input_a', mode = 'r') as fStream: RAW = fStream.read()
    lines, folds = preprocess_raw(RAW)
    mat = raw_to_mat(lines)
    mat = fold(mat, **folds[0])
    print(np.sum(mat))

    '''
        -- part two -- 
    '''
    for fold_inst in folds[1:]: mat = fold(mat, **fold_inst)
    prettyprint(mat)


if __name__ == '__main__': main()
