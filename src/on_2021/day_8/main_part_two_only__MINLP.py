import gekko

from on_2021.day_8.main_part_one_only import lines_ex_simple, lines_ex, lines


'''
    0:  0    1:  -   2:  0   3:  0   4:  -   5:  0   6:  0   7:  0   8:  0   9:  0
       1 2      - 2     - 2     - 2     1 2     1 -     1 -     - 2     1 2     1 2
        -        -       3       3       3       3       3       -       3       3
       4 5      - 5     4 -     - 5     - 5     - 5     4 5     - 5     4 5     - 5
        6        -       6       6       -       6       6       -       6       6
'''


number_to_IDXs: list[tuple[int, ...]] = [(0, 1, 2, 4, 5, 6),  # 0
                                         (2, 5),  # 1
                                         (0, 2, 3, 4, 6),  # 2
                                         (0, 2, 3, 5, 6),  # 3
                                         (1, 2, 3, 5),  # 4
                                         (0, 1, 3, 5, 6),  # 5
                                         (0, 1, 3, 4, 5, 6),  # 6
                                         (0, 2, 5),  # 7
                                         (0, 1, 2, 3, 4, 5, 6),  # 8
                                         (0, 1, 2, 3, 5, 6)]  # 9


def get(digit: int, sequence: str):
    assert len(sequence) == 7
    IDXs = number_to_IDXs[digit]
    return ''.join(sorted(sequence[idx] for idx in IDXs))


def verify_and_decode(sequence: str, line: str) -> tuple[bool, int]:
    assert len(sequence) == 7
    digits: list[str] = [get(i, sequence) for i in range(10)]
    _split = line.split(' | ')
    ins: list[str] = [''.join(sorted(entry)) for entry in _split[0].split()]
    outs: list[str] = [''.join(sorted(entry)) for entry in _split[1].split()]
    for in_number in ins:
        for digit in digits:
            if digit == in_number: break
        else: return False, -1
    final_number: str = ''
    for out in outs: final_number += str(digits.index(out))
    return True, int(final_number)


def crack_line(line: str, disp: bool = False) -> int:
    _split = line.split(' | ')
    line_numbers: list[list[str]] = [sorted(entry) for entry in _split[0].split()]

    m = gekko.GEKKO(remote = False)

    letters = {letter: m.Var(lb = 0, ub = 6, integer = True) for letter in 'abcdefg'}
    m.Equation([m.sos1(letters.values()) == i for i in range(7)])  # pairwise distinct
    val_to_possibilities = {6: [sum(number_to_IDXs[idx]) for idx in (0, 6, 9)],
                            2: [sum(number_to_IDXs[idx]) for idx in (1,)],
                            5: [sum(number_to_IDXs[idx]) for idx in (2, 3, 5)],
                            4: [sum(number_to_IDXs[idx]) for idx in (4,)],
                            3: [sum(number_to_IDXs[idx]) for idx in (7,)],
                            7: [sum(number_to_IDXs[idx]) for idx in (8,)]}
    for line_number in line_numbers:
        m.Equation(m.sos1(val_to_possibilities[len(line_number)]) == sum(letters[entry] for entry in line_number))

    m.Obj(0)  # feasibility problem
    m.options.SOLVER = 1
    # m.solver_options = ['minlp_branch_method 1']  # 1 = depth first, 2 = breadth first
    m.solve(disp = False)

    if disp: print(tuple(f'{key}:{letter.value}' for key, letter in letters.items()))
    sequence: str = ''.join(entry[0] for entry in sorted(letters.items(), key = lambda arg: arg[1][0]))
    return verify_and_decode(sequence, line)[1]


def main():
    # example 1
    assert verify_and_decode('abcdefg', lines_ex_simple[0]) == (False, -1)
    assert verify_and_decode('deafgbc', lines_ex_simple[0]) == (True, 5353)
    assert crack_line(lines_ex_simple[0]) == 5353

    # example 2
    asserts: list[int] = [8394, 9781, 1197, 9361, 4873, 8418, 4548, 1625, 8717, 4315]
    out_ex: int = 0
    for line_ex, assert_test in zip(lines_ex, asserts):
        summand_ex: int = crack_line(line_ex)
        assert summand_ex == assert_test
        out_ex += summand_ex
    assert out_ex == 61229

    # actual task
    out: int = 0
    for line in lines: out += crack_line(line)
    print(out)


if __name__ == '__main__': main()
