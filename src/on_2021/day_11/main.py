import numpy as np

import itertools


RAW_ex = '''5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526
'''


class OctopusField(object):
    data: np.ndarray = None
    flashes: int = None

    def __init__(self, lines: list[str]):
        self.flashes = 0
        self.data = np.array([[int(entry) for entry in line] for line in lines], dtype = int)

    def tick(self) -> None:
        self.data += 1
        self._check_flash()

    def _check_flash(self) -> None:
        new_flashes: int = int(np.sum(self.data > 9))
        self.flashes += new_flashes
        if new_flashes > 0: self._flashing()

    def _flashing(self) -> None:
        data_cp: np.ndarray = np.array(self.data, copy = True)
        for i, j in itertools.product(range(self.data.shape[0]), range(self.data.shape[1])):
            if data_cp[i, j] > 9:
                self.data[i, j] = 0
                Is, Js = [i], [j]
                if i > 0: Is.append(i - 1)
                if j > 0: Js.append(j - 1)
                if i + 1 < self.data.shape[0]: Is.append(i + 1)
                if j + 1 < self.data.shape[1]: Js.append(j + 1)
                for _i, _j in itertools.product(Is, Js):
                    if (_i == i) and (_j == j): continue
                    if self.data[_i, _j] > 0: self.data[_i, _j] += 1
        self._check_flash()

    def __str__(self) -> str:
        out: str = f'flashes so far: {self.flashes}'
        for i in range(self.data.shape[0]):
            out += '\n'
            for j in range(self.data.shape[1]):
                out += f'{self.data[i, j]}'
        return out


def main():
    '''
        -- part one --
    '''
    o_inst_ex = OctopusField(RAW_ex.splitlines())
    for _ in range(100): o_inst_ex.tick()
    assert o_inst_ex.flashes == 1656

    with open('input_a', mode = 'r') as fStream: RAW = fStream.read()
    o_inst = OctopusField(RAW.splitlines())
    for _ in range(100): o_inst.tick()
    print(o_inst.flashes)

    '''
        -- part two -- 
    '''
    o_inst_ex = OctopusField(RAW_ex.splitlines())  # require fresh
    idx_ex: int = 0
    while np.sum(o_inst_ex.data) > 0:
        o_inst_ex.tick()
        idx_ex += 1
    assert idx_ex == 195

    with open('input_a', mode = 'r') as fStream: RAW = fStream.read()
    o_inst = OctopusField(RAW.splitlines())  # require fresh
    idx: int = 0
    while np.sum(o_inst.data) > 0:
        o_inst.tick()
        idx += 1
    print(idx)


if __name__ == '__main__': main()
