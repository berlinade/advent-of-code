from itertools import product

from typing import TypeAlias, Optional


RAW_ex: str = '''Player 1 starting position: 4
Player 2 starting position: 8
'''


class Dice(object):
    mem: int = None
    rolls: int = None
    faces: int = None

    def __init__(self, faces: int = 100):
        self.mem = 0
        self.rolls = 0
        self.faces = faces

    def _single_roll(self) -> int:
        self.rolls += 1
        self.mem += 1
        if self.mem > self.faces: self.mem = 1
        return self.mem

    def roll(self, num: int) -> list[int]:
        return [self._single_roll() for _ in range(num)]


class Player(object):
    name: str = None
    points: int = None
    field: int = None

    def __init__(self, name: str, field: int, points: Optional[int] = None):
        self.name = name
        self.field = field - 1
        self.points = 0 if points is None else points

    def progress(self, eyes: int):
        self.field += eyes
        self.field %= 10
        self.points += 1 + self.field

    def __str__(self) -> str: return f'Player {self.name} - f: {self.field + 1}, p: {self.points}'

    def __repr__(self) -> str: return f'Player(name = {self.name}, field = {self.field}, points = {self.points})'


def game(players: list[int], score: int = 1000, verbose_light: bool = False, verbose: bool = False):
    players: list[Player] = [Player(name = str(idx), field = entry) for idx, entry in enumerate(players)]
    if verbose or verbose_light:
        for player in players: print(player)
    dice = Dice(faces = 100)
    carry_on: bool = True
    while carry_on:
        for player in players:
            player.progress(sum(dice.roll(3)))
            if verbose: print(player)
            if player.points >= score:
                carry_on = False
                break
    return min([player.points for player in players])*dice.rolls


state_t: TypeAlias = tuple[tuple[int, ...], tuple[int, ...]]  # 1st: points of every player; 2nd: field of every pl.
outcomes_t: TypeAlias = dict[state_t, int]  # maps state onto corresponding number of dimension


def quantum_game(player_fields: list[int], score: int = 21, verbose: bool = False):
    num_of_players: int = len(player_fields)

    relevant_rolls: dict[int, int] = dict()  # maps eys onto their corresponding repetitions
    for a, b, c in product(range(1, 4), range(1, 4), range(1, 4)):
        eyes = sum((a, b, c))
        if eyes in relevant_rolls: relevant_rolls[eyes] += 1
        else: relevant_rolls[eyes] = 1
    del eyes

    all_outcomes: outcomes_t = {(tuple(0 for _ in range(num_of_players)), tuple(player_fields)): 1}

    _id_round: int = 0
    carry_on: bool = True
    while carry_on:
        carry_on = False
        if verbose: print(f'quantum round: {(_id_round := _id_round + 1)}')

        for idx_player in range(num_of_players):
            new_outcomes: outcomes_t = dict()
            for state_round, dimensions in all_outcomes.items():
                if max(state_round[0]) >= score: continue
                carry_on = True  # i.e. there is still at least one open game left

                # actual player turn
                for eyes, reps in relevant_rolls.items():
                    # spawn new dimensions
                    state_turn: tuple[list[int], list[int]] = (list(state_round[0]), list(state_round[1]))
                    # compute field
                    state_turn[1][idx_player] += eyes
                    while state_turn[1][idx_player] > 10: state_turn[1][idx_player] -= 10
                    # compute points
                    state_turn[0][idx_player] += state_turn[1][idx_player]

                    state_turn: state_t = (tuple(state_turn[0]), tuple(state_turn[1]))

                    if state_turn in new_outcomes: new_outcomes[state_turn] += dimensions*reps
                    else: new_outcomes[state_turn] = dimensions*reps

                all_outcomes[state_round] = 0  # reset all corresponding dimensions, since they have just been handled
            for state, dimensions in new_outcomes.items():
                if state in all_outcomes: all_outcomes[state] += dimensions
                else: all_outcomes[state] = dimensions
            all_outcomes = {state: dimensions for state, dimensions in all_outcomes.items() if dimensions > 0}
            del new_outcomes

    wins: list[int] = [0 for _ in range(num_of_players)]
    for state, dimensions in all_outcomes.items():
        wins[state[0].index(max(state[0]))] += dimensions
    return wins


def main():
    '''
        -- part one --
    '''
    lines_ex = RAW_ex.splitlines()
    players_ex: list[int] = [int(line_ex.split()[-1]) for line_ex in lines_ex]
    assert game(players_ex) == 739785

    with open('input_a', mode = 'r') as fStream: RAW = fStream.read()
    lines = RAW.splitlines()
    players: list[int] = [int(line.split()[-1]) for line in lines]
    print(game(players))

    '''
        -- part two --
    '''
    assert quantum_game(players_ex) == [444356092776315, 341960390180808]

    print(max(quantum_game(players)))


if __name__ == '__main__': main()
