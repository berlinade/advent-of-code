from on_2021.day_3.main_part_one_only import RAW_example
from on_2021.day_3.main_part_one_only import bin_list_2_int


class Tag(object):
    result: int

    def grab(self,
             left: list[list[int]],
             right: list[list[int]]): pass

    def __call__(self, balance: int): pass


class OxygenGenerator(Tag):

    def grab(self,
             left: list[list[int]],
             right: list[list[int]]):
        if len(left) > 0: self.result = bin_list_2_int(left[0])
        else: self.result = bin_list_2_int(right[0])

    def __call__(self, balance: int):
        if balance >= 0: return 'left'
        if balance < 0: return 'right'
        return 'center'


class Co2Scrubber(Tag):

    def grab(self,
             left: list[list[int]],
             right: list[list[int]]):
        if len(right) > 0: self.result = bin_list_2_int(right[0])
        else: self.result = bin_list_2_int(left[0])

    def __call__(self, balance: int):
        if balance >= 0: return 'right'
        if balance < 0: return 'left'
        return 'center'


def tree_search(bins: list[list[int]],
                tags: list[Tag],
                idx: int = 0) -> None:
    if len(tags) == 0: return None
    left: list[list[int]] = []
    right: list[list[int]] = []
    for _bin in bins:
        b = _bin[idx]
        if b: left.append(_bin)
        else: right.append(_bin)
    balance: int = len(left) - len(right)
    tags_left: list[Tag] = []
    tags_right: list[Tag] = []
    for tag in tags:
        if (len(bins[0]) - 1 == idx) or (len(bins) == 1):
            tag.grab(left, right)
        else:
            match tag(balance):
                case 'left': tags_left.append(tag)
                case 'right': tags_right.append(tag)
                case _: tag.grab(left, right)
    tree_search(left, tags_left, idx + 1)
    tree_search(right, tags_right, idx + 1)


def main():
    bins_example: list[list[int]] = []
    for line in RAW_example.splitlines():
        bins_example.append([int(b) for b in line])
    o_tag_example = OxygenGenerator()
    c_tag_example = Co2Scrubber()
    tree_search(bins_example, [o_tag_example, c_tag_example])
    assert o_tag_example.result*c_tag_example.result == 230

    bins: list[list[int]] = []
    with open('input_a', mode = 'r') as fStream:
        for line in fStream.readlines():
            bins.append([int(b) for b in line[:-1]])
    o_tag = OxygenGenerator()
    c_tag = Co2Scrubber()
    tree_search(bins, [o_tag, c_tag])
    print(o_tag.result*c_tag.result)


if __name__ == '__main__': main()
