from typing import Union


RAW_ex: str = '''2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8
'''

SOL_ex_part_1: int = 2

SOL_ex_part_2: int = 4


with open('input', mode = 'r') as fStream: RAW: str = fStream.read()
