from on_2022.day_6.source_loader import RAW_ex, SOL_ex_part_1, RAW
from on_2022.day_6.source_loader import RAW_ex_2, SOL_ex_2_part_1


_width: int = 4


def part_1(raw: str) -> int:
    signal: str = raw.strip()
    finger_print: dict[str, int] = dict()
    for letter in signal[:_width]: finger_print[letter] = finger_print.get(letter, 0) + 1

    idx: int = -1
    for idx, (drop_letter, letter) in enumerate(zip(signal, signal[_width:]), start = _width):
        if len(finger_print) > _width: raise AssertionError('!')
        elif len(finger_print) == _width: break

        if finger_print[drop_letter] > 1: finger_print[drop_letter] -= 1
        else: del finger_print[drop_letter]
        finger_print[letter] = finger_print.get(letter, 0) + 1
    else: raise AssertionError('signal exhausted!')

    return idx


def test():
    out = part_1(RAW_ex)
    assert out == SOL_ex_part_1

    out = part_1(RAW_ex_2)
    assert out == SOL_ex_2_part_1


def main():
    test()
    print(part_1(RAW))


if __name__ == '__main__': main()
