from typing import Union


RAW_ex: str = '''mjqjpqmgbljsphdztnvjfqwrcgsmlb
'''

SOL_ex_part_1: int = 7

SOL_ex_part_2: int = 19


RAW_ex_2: str = '''bvwbjplbgvbhsrlpgdmjqwftvncz
'''

SOL_ex_2_part_1: int = 5

SOL_ex_2_part_2: int = 23


with open('input', mode = 'r') as fStream: RAW: str = fStream.read()
