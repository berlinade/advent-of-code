from on_2022.day_3.source_loader import RAW_ex, SOL_ex_part_2, RAW


def part_2(raw: str) -> int:
    findings: list[str] = []
    lines: list[str] = raw.splitlines()
    for line0, line1, line2 in zip(lines[::3], lines[1::3], lines[2::3]):
        sub_findings: list[str] = []
        for letter in line0:
            if letter in line1:
                if letter in sub_findings: pass
                else: sub_findings.append(letter)
        match_idx: int = -1
        for idx, sub_finding in enumerate(sub_findings):
            if sub_finding in line2:
                if match_idx >= 0: raise AssertionError('!')
                match_idx = idx
        findings.append(sub_findings[match_idx])
    summ: int = 0
    for finding in findings:
        if finding == finding.lower(): summ += 1 + ord(finding) - ord('a')
        elif finding == finding.upper(): summ += 27 + ord(finding) - ord('A')
        else: raise AssertionError('!')

    return summ


def test():
    out = part_2(RAW_ex)
    assert out == SOL_ex_part_2


def main():
    test()
    print(part_2(RAW))


if __name__ == '__main__': main()
