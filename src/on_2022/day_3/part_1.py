from on_2022.day_3.source_loader import RAW_ex, SOL_ex_part_1, RAW


def part_1(raw: str) -> int:
    findings: list[str] = []
    for line in raw.splitlines():
        assert len(line) % 2 == 0
        mid: int = len(line)//2
        comp1 = line[:mid]
        comp2 = line[mid:]
        finding: str = ''
        for letter in comp1:
            if letter in comp2:
                if finding == '': finding = letter
                elif finding == letter: pass
                else: raise AssertionError('!')
        findings.append(finding)
    summ: int = 0
    for finding in findings:
        if finding == finding.lower(): summ += 1 + ord(finding) - ord('a')
        elif finding == finding.upper(): summ += 27 + ord(finding) - ord('A')
        else: raise AssertionError('!')

    return summ


def test():
    out = part_1(RAW_ex)
    assert out == SOL_ex_part_1


def main():
    test()
    print(part_1(RAW))


if __name__ == '__main__': main()
