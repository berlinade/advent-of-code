from typing import Union


def raw_parser(raw: str) -> tuple[list[list[str]], list[list[int]]]:
    lines: list[str] = raw.splitlines()

    # stage 1
    j: int = 1
    mem: int = -1
    for line in lines:
        try:
            int(line[j])
            while mem := int(line[j]): j += 4
            break
        except (ValueError, IndexError): continue

    stacks: list[list[str]] = [[] for _ in range(mem)]

    # stage 2
    idx: int = -1
    for idx, line in enumerate(lines):
        try:
            int(line[1])
            break
        except ValueError: pass
        j = 1
        jj: int = 0
        letter: str = ''
        while True:
            try: letter = line[j]
            except ValueError: pass
            except IndexError: break
            if letter != ' ': stacks[jj].insert(0, letter)
            j += 4
            jj += 1

    # stage 3
    idx += 2
    moves: list[list[int]] = [[]]
    for line in lines[idx:]:
        start: int = 0
        stop: int = 1
        while True:
            try: int(line[start])
            except ValueError:
                start = stop
                stop = start + 1
                continue
            except IndexError: break
            while True:
                try: int(line[stop])
                except (ValueError, IndexError):
                    moves[-1].append(int(line[start:stop]))
                    start = stop
                    stop = start + 1
                    break
                stop += 1
        moves.append([])

    return stacks, moves[:-1]


RAW_ex: str = '''    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
'''

SOL_ex_part_1: str = 'CMZ'

SOL_ex_part_2: str = 'MCD'


with open('input', mode = 'r') as fStream: RAW: str = fStream.read()
