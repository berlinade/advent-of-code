from on_2022.day_5.source_loader import RAW_ex, SOL_ex_part_1, RAW, raw_parser


def stacks_move_executer__cratemover_9000(stacks: list[list[str]], move: list[int]) -> None:
    assert len(move) == 3
    take: list[str] = stacks[move[1] - 1][-move[0]:]  # take
    stacks[move[1] - 1] = stacks[move[1] - 1][:-move[0]]  # shrink origin
    take.reverse()
    stacks[move[2] - 1].extend(take)  # drop


def part_1(raw: str) -> str:
    stacks, moves = raw_parser(raw)

    for move in moves: stacks_move_executer__cratemover_9000(stacks, move)
    final_top: str = ''
    for stack in stacks: final_top += stack[-1]

    return final_top


def test():
    out = part_1(RAW_ex)
    assert out == SOL_ex_part_1


def main():
    test()
    print(part_1(RAW))


if __name__ == '__main__': main()
