from on_2022.day_1.source_loader import RAW


def main():
    num_of_elves = 1
    for line in RAW.split('\n'):
        try: int(line)
        except ValueError: num_of_elves += 1
    print(num_of_elves)


if __name__ == '__main__': main()
