from on_2022.day_1.source_loader import RAW_ex, RAW, SOL_ex_part_1


def part_1(raw: str):
    summ_curr: int = 0
    idx_curr: int = 1
    summ_max: int = -1
    idx_max: int = -1
    for line in raw.split('\n'):
        try: summ_curr += int(line)
        except ValueError:
            if summ_curr > summ_max:
                summ_max = summ_curr
                idx_max = idx_curr
            summ_curr = 0
            idx_curr += 1

    return summ_max, idx_max


def test():
    out = part_1(RAW_ex)
    assert out[0] == SOL_ex_part_1[0]
    assert out[1] == SOL_ex_part_1[1]


def main():
    test()
    print(part_1(RAW))


if __name__ == '__main__': main()
