from typing import Union


ROCK: str = 'Rock'
PAPER: str = 'PAPER'
SCISSORS: str = 'Scissors'

beats: dict[str, str] = {ROCK: SCISSORS, SCISSORS: PAPER, PAPER: ROCK}
fails: dict[str, str] = {SCISSORS: ROCK, PAPER: SCISSORS, ROCK: PAPER}

LEFT: str = 'left wins!'
DRAW: str = 'draw'
RIGHT: str = 'right wins!'


def simulate_round(*,
                   left: str | None = None,
                   right: str | None = None,
                   result: str | None = None) -> tuple[str, str, str]:
    check: int = 0
    for arg in (left, right, result):
        if arg is None: check += 1
    if check != 1: raise AssertionError('need at least 2 no-None args!')

    if left is None:
        if result == LEFT: return fails[right], right, result
        if result == RIGHT: return beats[right], right, result
        if result == DRAW: return right, right, result
        raise AssertionError(f'result in ("{LEFT}", "{RIGHT}", "{DRAW}")!')
    elif right is None:
        if result == LEFT: return left, beats[left], result
        if result == RIGHT: return left, fails[left], result
        if result == DRAW: return left, left, result
        raise AssertionError(f'result in ("{LEFT}", "{RIGHT}", "{DRAW}")!')
    if beats[left] == right: return left, right, LEFT
    if beats[right] == left: return left, right, RIGHT
    if left == right: return left, right, DRAW
    raise AssertionError(f'left, right in ({ROCK}, {PAPER}, {SCISSORS})!')


LOOSE: str = 'loose'
WIN: str = 'win'
points = {ROCK: 1, PAPER: 2, SCISSORS: 3, LOOSE: 0, DRAW: 3, WIN: 6}


RAW_ex: str = '''A Y
B X
C Z
'''

SOL_ex_part_1: int = 15

SOL_ex_part_2: int = 12


with open('input', mode = 'r') as fStream: RAW: str = fStream.read()
