from on_2022.day_2.source_loader import RAW_ex, SOL_ex_part_2, RAW
from on_2022.day_2.source_loader import ROCK, PAPER, SCISSORS, simulate_round
from on_2022.day_2.source_loader import points, RIGHT, WIN, LEFT, LOOSE, DRAW


figure_map = dict(A = ROCK, B = PAPER, C = SCISSORS)


def part_2(raw: str) -> int:
    rights_win_condtion = {RIGHT: WIN, LEFT: LOOSE, DRAW: DRAW}
    strategy = dict(X = LEFT, Y = DRAW, Z = RIGHT)

    score: int = 0
    for line in raw.splitlines():
        left, right = line.split(' ')

        left = figure_map[left]
        result = strategy[right]

        _, right, _ = simulate_round(left = left, result = result)
        score += points[right]
        score += points[rights_win_condtion[result]]
    return score


def test():
    out = part_2(RAW_ex)
    assert out == SOL_ex_part_2


def main():
    test()
    print(part_2(RAW))


if __name__ == '__main__': main()
